	
package rest;
	
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Category;

//import org.w3c.dom.ls.LSInput;

//import com.jayway.restassured.internal.http.Status;

import domain.Product;

	
	@Path("/product")
	@Stateless
	public class ProductResources {
	
			
		@PersistenceContext
		EntityManager em;
		
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public List<Product> getAll()
		{
			return em.createNamedQuery("product.all", Product.class).getResultList();
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		public Response Add(Product product){
			
			if(product.getCategory() == null) {
				return Response.status(400).build();
			}
			
			em.persist(product);
			return Response.ok(product.getId()).build();
		}
		
		@GET
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response get(@PathParam("id") int id){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", id)
					.getSingleResult();
			if(result==null){
				return Response.ok("Nie znaleziono rekordow!").build();
			}
			return Response.ok(result).build();
		}
		
		@GET
		@Path("/price")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Product> get(@QueryParam("priceStart") float priceStart, @QueryParam("priceStop") float priceStop){
			
			
			System.out.println(priceStart);
			System.out.println(priceStop);
			
			List<Product> result = em.createNamedQuery("product.price", Product.class)
					.setParameter("priceStart", priceStart)
					.setParameter("priceStop", priceStop)
					.getResultList();
			return result;
		}
		
		@GET
		@Path("/category")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Product> get(@QueryParam("category") Category category){

			List<Product> result = em.createNamedQuery("product.category", Product.class)
					.setParameter("category", category)
					.getResultList();
			return result;
		}
		
		@PUT
		@Path("/{id}")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response update(@PathParam("id") int id, Product p){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", id)
					.getSingleResult();
			if(result==null)
				return Response.status(404).build();
			result.setName(p.getName());
			result.setCategory(p.getCategory());
			result.setPrice(p.getPrice());
			em.persist(result);
			return Response.ok().build();
		}
		@DELETE
		@Path("/{id}")
		public Response delete(@PathParam("id") int id){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", id)
					.getSingleResult();
			if(result==null)
				return Response.status(404).build();
			em.remove(result);
			return Response.ok().build();
		}
	}
		
				
		
	
	